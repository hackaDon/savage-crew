# Fiche descriptive du projet
![Image](https://i.imgur.com/4veNC78.png)

([AdopteUnBenevole.com](https://hackadon.frama.io/savage-crew/adopte_un_benevole/index.html))

([Accéder aux maquettes du projet](https://invis.io/FZPEH9QXQ3W))

## Adopte un bénévole
L'objectif d'**Adopte un bénévole** est de mettre en relation le plus facilement possible les potentiels bénévoles avec des projets solidaires et/ou des associations en fonction de leurs compétences, de leurs diponibilités et de leur localisation.
Mais permet aussi aux bénévoles de faire des dons gratuits (visonnage de vidéos) ou via des dons classiques (virement paypal, bancaires, ...).

## Acquisition 
Sensibilisation au bénévolat en proposant des ateliers ainsi que des conférences dans des écoles, collèges, lycées et universités, en expliquant le fonctionnement de la plateforme **Adopte un bénévole**. 
Proposition de ressources et de tutoriels pour améliorer les descriptions des projets mis en avant par les associations sur la plateforme. 

## Qu'est-ce que **Adopte un bénévole** ?
C'est une plateforme de mise en relation entre bénévoles et associations. Nous proposons deux solutions **Adopte un bénévole** et  **Adopte une association**. 

### Adopte un bénévole 
Il s'agit de la solution que nous proposons pour les associations. Elles peuvent poster des projets solidaires en décrivant leurs besoins en matière de compétences et de durée d'investissement. L'association valorise l'engagement des bénévoles en offrant des badges ([Open Badge](https://openbadges.org/)) ou encore en partageant les profils des meilleurs bénévoles.

### Adopte une association
Il s'agit de la solution que nous proposons pour les bénévoles. Ils peuvent chercher un projet solidaire selon les critères de leurs choix (localisation, domaines, durée, ...) et peuvent donc y participer. Les bénévoles ont aussi la possibilité de faire des dons gratuits (en visionnant des publicités), ou des dons classiques (via virement paypal, bancaire, ...). L'expérience des bénévoles sera valorisée par l'obtention d'un badge ([Open Badge](https://openbadges.org/)) qui pourra être mis en avant sur LinkedIn ou sur son site web personnel, ou dans sa signature d'e-mail.