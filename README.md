# hackaDon 2018 Savage Crew

Vous y retrouverez trois dossiers **docs**, **produit** et **Api-platform** qui peuvent être utilisés par les différents profils : [**Développeur**](#devéloppeur), [**Design**](#design) et [**Market**](#market).

# Avant propos
### Le dossier docs

Ce dossier permet de publier votre documentation ici : https://hackaDon.frama.io/savage-crew/

Pour ce faire, c'est très simple :
* Créer vos documents en [markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* Faire un push sur la branche master
    * C'est l'intégration continue (gitlab-ci.yml) qui s'occupe de la publication automatique.
    * On utilise la librairie [MkDocs](https://www.mkdocs.org/) pour la génération des pages HTML.
# Les différents profils

## Devéloppeur
Vous avez plusieurs options de développement :
##### 1) Utilisation du dossier docs pour avoir un client léger
Le dossier **docs** permet de publier vos fichiers ici : https://hackaDon.frama.io/savage-crew/. Il est donc possible d'y publier vos fichiers HTML, JS et CSS pour avoir un client léger.
##### 2) Utilisation du dossier Api-platform pour un développement plus poussé
Vous avez choisi une application web/api comme projet ? Ça tombe bien ! Le dossier Api-platform est votre dossier de développement prêt à l'emploi.

Avant toute chose, récuperer votre repository :

```sh
git clone git@framagit.org:hackaDon/savage-crew.git
```

Puis configurer votre environnement avec les commandes suivantes :

_Cela permet de créer les fichiers des variables d'environnement_ 
```sh
cd savage-crew/Api-platform
cp api/.env.dist api/.env
cp admin/.env.dist admin/.env
cp client/.env.dist client/.env
```

Ensuite, rien de plus simple que de faire un `docker-compose up` une fois dans le dossier Api-Platform. (vous n'avez pas docker-compose, ni même docker ? [cliquez-ici](https://docs.docker.com/compose/install/))

Suite à cela, en local, vous aurez :

* Une api : https://localhost:8443/
* Une admin (pour CRUD vos données) : https://localhost:444
* Un client : https://localhost/

Ensemble de documentations utiles : 

* Api Platform : https://api-platform.com/docs/distribution
* Docker : https://www.docker.com/

## Design

1) Vous pouvez réaliser vos maquettes sur votre outil favori puis les mettre dans le dossier **docs** pour les partager à tous sur votre page statique : https://hackaDon.frama.io/savage-crew/

2) Le dossier **Produit**, quant à lui, contient deux sous dossiers et celui qui va vous intéresser est **Test BDD**.
	* Ici, vous pouvez déposer vos tests BDD. Qu'est-ce qu'un test BDD ? [cliquez-ici](https://en.wikipedia.org/wiki/Behavior-driven_development)
_Vous trouverez un exemple de Test BDD concernant [l'ARRONDI](https://www.larrondi.org/) dans le dossier._

## Market

1) Vous pouvez mettre votre documentation dans le dossier **docs** pour les partager à tous sur votre page statique : https://hackaDon.frama.io/savage-crew/

2) Le dossier **Produit**, quant à lui, contient deux sous dossiers et celui qui va vous intéresser est **User Story**.
	* Ici, vous pouvez déposer vos User Stories. Qu'est-ce qu'une User Story ? [cliquez-ici](https://en.wikipedia.org/wiki/User_story)
_Vous trouverez un exemple de User Story concernant [l'ARRONDI](https://www.larrondi.org/) dans le dossier._
3) Vous serez aussi amenés à créer des [issues](https://framagit.org/hackaDon/savage-crew/issues) et un [backlog](https://framagit.org/hackaDon/savage-crew/boards) priorisé et versionné en accord avec les Devéloppeurs

**À vous de jouer ! :D**

