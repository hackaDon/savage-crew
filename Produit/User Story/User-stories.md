# User stories
## En tant que bénévole
### Inscription et Connexion

En tant que bénévole je peux m'inscrire rapidement pour ne pas perdre de temps

En tant que bénévole je peux me connecter rapidement sans perdre de temps

### Création du Profil

En tant que bénévole je peux compléter/remplir mon profil rapidement pour ne pas perdre de temps

En tant que bénévole je peux étoffer mon profil pour être le plus précis possible

### Don
En tant que bénévole je peux faire des dons pour participer de manière financière

En tant que bénévole je peux regarder une publicité pour faire un don gratuit

En tant que consommateur je veux reverser un "cashback" aux associations de mon choix lorsque je fais un achat 
en ligne

### Rechercher une association ou un projet solidaire

En tant que bénévole je peux m'engager dans des projets qui me correspondent pour me sentir le plus utile possible

En tant que bénévole je peux filtrer les différents résultats d'actions bénévoles par durée, lieux et compétences pour trouver celle qui me correspond le mieux

### Intégrer un projet

En tant que bénévole je peux sélectionner un ou plusieurs projets pour m'investir autant que je le veuille

En tant que bénévole je peux communiquer avec les associations de mon choix pour discuter des événements

### Notifications
En tant que bénévole je peux recevoir des notifications pour savoir si un projet correspond à mes préférences

En tant que bénévole je peux recevoir des notifications pour savoir si mon intégration à un projet est prise en compte

En tant que bénévole je peux désactiver les notifications de mon choix pour limiter le flot de notifications que je peux recevoir quotidiennement

### Reconnaissance

En tant que bénévole je peux enrichir mon CV avec la démarcation des projet auxquels j'ai participé pour montrer mon activité et ma volonté d'agir

En tant que bénévole je peux faire reconnaître mon implication pour partager mon activité et mes motivations

## En tant qu'Association

### Inscription et connexion 

En tant qu'association je peux me connecter rapidement et simplement pour ne pas perdre de temps

En tant qu'association je peux m'inscrire rapidement et simplement pour ne pas perdre de temps

En tant qu'association je peux compléter mon profil pour étoffer le message que je veux faire passer et mes motivations

### Développer et améliorer mon projet 

En tant qu'association je peux accéder à des ressources pour m'aider à développer mon projet

En tant qu'association je peux accéder à des ressources pour améliorer ma description de projet 

### Soumettre un projet 

En tant qu'association je peux créer un projet pour le rendre public aux yeux des bénévoles  

En tant qu'association je peux modifier mon projet pour corriger/changer des détails pour l'organisation

En tant qu'association je peux supprimer mon projet pour l'annulé en cas d’empêchements

En tant qu'association je peux mettre en avant les compétences que je recherche pour permettre aux bénévoles de trouver plus facilement ce qui leur correspondent 

### Notifications 

En tant qu'association je peux recevoir une notification lorsque qu'un autre utilisateur me contacte pour communiquer avec lui

En tant qu'association je peux recevoir une notification lorsqu'un bénévole veut intégrer un projet pour découvrir son profil 

En tant qu'association je peux désactiver les notifications de mon choix pour limiter le nombre trop important de notifications

En tant qu'association je peux recevoir une notifications quand j'ai la possibilités de reconnaître l'investissement d'un bénévole dans mon projet pour le remercier de son engagement

### Reconnaissance du projet 

En tant qu'association je peux reconnaître l'engagement des bénévoles pour les remercier de leur investissement

En tant qu'association je peux valoriser le profil des bénévoles pour les remercier de leur investissement

En tant qu'association je cherche à mettre des profils en avant pour montrer les bénévoles les plus investis